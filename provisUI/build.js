var fs = require('fs');
var browserify = require('browserify');
var watchify = require('watchify');
var less = require('less');
var watch = require('watch');
var path = require('path');
switch(process.argv[2]){
	case "watchAll":
		watchJS(0);
		watchCSS();
		break;
	case "watchJS":
		console.log("watchJS");
		break;
	case "watchCSS":
		console.log("watchCSS");
		break;
	case "compileAll":
		console.log("compileAll");
		break;
	case "compileJS":
		console.log("compileJS");
		break;
	case "compileCSS":
		buildCSS();
		break;
	default:
		console.error("invalid command");
		break;
}

function watchJS(choice){
		var b = browserify({
	  entries: ['./src/main.js'],
	  cache: {},
	  packageCache: {},
	  plugin: [watchify]
	});

	b.on('update', bundle);
	bundle();
	console.log("build completed");
	function bundle() {
		b.bundle().pipe(fs.createWriteStream('./src/main.min.js'));
	}
}

function buildCSS(){
	less.render(fs.readFileSync('./src/main.less','utf8'),
							{filename:'main.less',compress:true},
							function(error,output){
								fs.writeFileSync('./src/main.min.css',output.css);
							});
}

function watchCSS(){
	watch.watchTree('./src',function(f,current,previous){
		if((f.substr && f.substr(f.length-5) === ".less")){
			buildCSS();
		}
	})
}
