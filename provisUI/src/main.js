var app = angular.module('app', ["ui.router","ngCookies","ui.bootstrap"]);
require("./services/loginServices")(app);
require("./pages/navbar")(app);
var routes=[];
app.value("tokenCookie","provisToken");
routes.push(require("./pages/login/loginController")(app));
routes.push(require("./pages/postLogin/dashboard/dashBoardController")(app));
routes.push(require("./pages/postLogin/violationDetail/violationDetail")(app));
app.config(function($stateProvider, $urlRouterProvider,$locationProvider){
  $urlRouterProvider.otherwise("/");
  for(var i = 0; i < routes.length; i++){
    $stateProvider.state(routes[i].name,routes[i].routeDetail);
  }
});

app.run(["$rootScope","$state","$cookies","tokenCookie",function($rootScope,$state,$cookies,tokenCookie){
  $rootScope.$on("$stateChangeStart",function(event, toState){
    if(toState.secured){
      if($cookies.get(tokenCookie)=== undefined){
        event.preventDefault();
        $state.go("login");
      }
    }
  })
}]);
