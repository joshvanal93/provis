module.exports = function(app){
  app.controller('navbarController',["$scope","$rootScope","$cookies","tokenCookie","$state",function($scope,$rootScope,$cookies,tokenCookie,$state){
    $scope.navCollapsed = true;
    $scope.signout = function(){
      $cookies.remove(tokenCookie,{domain:window.location.hostname});
      $state.go("login");
    }
  }]);
}
