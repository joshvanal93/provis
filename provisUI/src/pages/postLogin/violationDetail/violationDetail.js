module.exports = function(app){
  app.controller('violationDetailController',[function(){
  }]);
  return{
    name:'dashboard.violationDetail',
    routeDetail:{
      url:'violation/:id',
      views:{
        'body@':{
          templateUrl:"./pages/postLogin/violationDetail/violationDetail.html",
          controller:"violationDetailController"
        }
      },
      secured:true
    }
  };
}
