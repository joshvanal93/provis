module.exports = function(app){
  app.controller('dashBoardController',["$scope","$state",function($scope,$state){
    $scope.gotoViolation = function(violationInfo){
      $state.go("dashboard.violationDetail",{violation:violationInfo});
    }
  }]);
  return{
    name:'dashboard',
    routeDetail:{
      url:'/',
      views:{
        'body@':{
          templateUrl:"./pages/postLogin/dashboard/index.html",
          controller:"dashBoardController"
        },
        'nav@':{
          templateUrl:"./pages/navbar.html",
          controller:"navbarController"
        }
      },
      secured:true
    }
  };
}
