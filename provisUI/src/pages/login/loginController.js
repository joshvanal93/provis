module.exports = function(app){
  app.controller('loginController',['$scope','LoginService','$state',function($scope,LoginService,$state){
    $scope.userInfo ={
      userName:"",
      password:"",
    }
    $scope.login = function(){
      $scope.error = false;
      LoginService.getLoginCooke($scope.userInfo).then(function(resp){
        $state.go('dashboard');
      },function(){$scope.error = true});
    };
    window.scope = $scope;
  }]);
  return{
    name:'login',
    routeDetail:{
      url:'/login',
      views:{
        'body@':{
          templateUrl:"./pages/login/index.html",
          controller:"loginController"
        }
      },
      secured:false
    }
  };
}
