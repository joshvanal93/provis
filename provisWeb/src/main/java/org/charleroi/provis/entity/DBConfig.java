package org.charleroi.provis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="dbconfig",schema="provis")
public class DBConfig {

	@Id
	@Column(name="dbconfig_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="dbconfig_key")
	private String key ;
	
	@Column(name="dbconfig_value")
	private String value;
	
	@Column(name="dbconfig_status")
	private String status;
	
	public DBConfig(){
		super();
	}
	
	public DBConfig(Long id, String key, String value, String status) {
		super();
		this.id = id;
		this.key = key;
		this.value = value;
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
