package org.charleroi.provis.entity;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="violation",schema="provis")
public class Violation {
	@Id
	@Column(name="VIO_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@JoinColumn(name="PARCEL_ID")
	@ManyToOne(cascade=CascadeType.ALL)
	private Pacel parcel;
	
	@JoinColumn(name="violation_creator")
	@ManyToOne(cascade=CascadeType.ALL)
	private User creator;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "violation")
	private List<ViolationDetail> violationDetails;
	
	@JoinColumn(name="violation_modified_id")
	@ManyToOne(cascade=CascadeType.ALL)
	private User modifiedBy;
	
	@Column(name="violation_creation_date")
	private Date creationDate;
	
	@Column(name="violation_modified_ts")
	private Timestamp modifiedTS;
 
	public Violation() {
		super();
	}

	public Violation(Long id, Pacel parcel, User creator,
			List<ViolationDetail> violationDetails, User modifiedBy,
			Date creationDate, Timestamp modifiedTS) {
		super();
		this.id = id;
		this.parcel = parcel;
		this.creator = creator;
		this.violationDetails = violationDetails;
		this.modifiedBy = modifiedBy;
		this.creationDate = creationDate;
		this.modifiedTS = modifiedTS;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pacel getParcel() {
		return parcel;
	}

	public void setParcel(Pacel parcel) {
		this.parcel = parcel;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public User getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(User modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Timestamp getModifiedTS() {
		return modifiedTS;
	}

	public void setModifiedTS(Timestamp modifiedTS) {
		this.modifiedTS = modifiedTS;
	}

	public List<ViolationDetail> getViolationDetails() {
		return violationDetails;
	}

	public void setViolationDetails(List<ViolationDetail> violationDetails) {
		this.violationDetails = violationDetails;
	}
	
}
