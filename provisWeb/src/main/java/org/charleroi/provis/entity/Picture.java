package org.charleroi.provis.entity;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "picture", schema = "provis")
public class Picture {
	@Id
	@Column(name = "photo_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "vioDetail_id")
	private ViolationDetail violationDetail;

	@Column(name = "photo_blob")
	@Lob
	private Blob image;

	@Column(name = "photo_desc")
	private String desc;

	public Picture() {
		super();
	}

	public Picture(Long id, ViolationDetail violationDetail, Blob image,
			String desc) {
		super();
		this.id = id;
		this.violationDetail = violationDetail;
		this.image = image;
		this.desc = desc;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Blob getImage() {
		return image;
	}

	public void setImage(Blob image) {
		this.image = image;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public ViolationDetail getViolationDetail() {
		return violationDetail;
	}

	public void setViolationDetail(ViolationDetail violationDetail) {
		this.violationDetail = violationDetail;
	}

}
