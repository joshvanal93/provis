package org.charleroi.provis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "users", schema = "provis")
public class User {
	@Id
	@Column(name = "user_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long user_id;

	@Column(name = "user_login")
	private String user_login;

	@Column(name = "user_password")
	private String user_password;

	@Column(name = "user_firstName")
	private String user_firstName;

	@Column(name = "user_lastName")
	private String user_lastName;

	@Column(name = "user_email")
	private String user_email;

	@OneToOne
	@JoinColumn(name = "user_type")
	private UserType user_type;

	public User() {
		super();
	}

	public User(Long user_id, String user_login, String user_password,
			String user_firstName, String user_lastName, String user_email,
			UserType user_type) {
		super();
		this.user_id = user_id;
		this.user_login = user_login;
		this.user_password = user_password;
		this.user_firstName = user_firstName;
		this.user_lastName = user_lastName;
		this.user_email = user_email;
		this.user_type = user_type;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

	public String getUser_login() {
		return user_login;
	}

	public void setUser_login(String user_login) {
		this.user_login = user_login;
	}
	@JsonIgnore
	public String getUser_password() {
		return user_password;
	}
	
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}

	public String getUser_firstName() {
		return user_firstName;
	}

	public void setUser_firstName(String user_firstName) {
		this.user_firstName = user_firstName;
	}

	public String getUser_lastName() {
		return user_lastName;
	}

	public void setUser_lastName(String user_lastName) {
		this.user_lastName = user_lastName;
	}

	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	
	public UserType getUser_type() {
		return user_type;
	}

	public void setUser_type(UserType user_type) {
		this.user_type = user_type;
	}

	@Override
	public String toString() {
		return "User [user_id=" + user_id + ", user_login=" + user_login
				+ ", user_password=" + user_password + ", user_firstName="
				+ user_firstName + ", user_lastName=" + user_lastName
				+ ", user_email=" + user_email + ", user_type=" + user_type.toString()
				+ "]";
	}

}
