package org.charleroi.provis.entity;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="violation_detail",schema="provis")
public class ViolationDetail {
	@Id
	@Column(name = "violationDetail_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne
	@JoinColumn(name="violationDetail_codeId")
	private Code code;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="violationDetail_violationId")
	private Violation violation;
	
	@OneToMany(fetch=FetchType.EAGER,mappedBy="violationDetail")
	private List<Picture> pictures;
	
	@Column(name="violationDetail_description")
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "violationDetail_creator")
	private User creator;
	
	@ManyToOne
	@JoinColumn(name = "violationDetail_modified_id")
	private User modifiedBy;
	
	@Column(name="violationDetail_creation_date")
	private Timestamp creationDate;
	
	@Column(name="violationDetail_modified_date")
	private Timestamp modifiedTimeStamp;

	public ViolationDetail() {
		super();
	}

	public ViolationDetail(Long id, Code code, Violation violation,
			List<Picture> pictures, String description, User creator,
			User modifiedBy, Timestamp creationDate, Timestamp modifiedTimeStamp) {
		super();
		this.id = id;
		this.code = code;
		this.violation = violation;
		this.pictures = pictures;
		this.description = description;
		this.creator = creator;
		this.modifiedBy = modifiedBy;
		this.creationDate = creationDate;
		this.modifiedTimeStamp = modifiedTimeStamp;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Code getCode() {
		return code;
	}

	public void setCode(Code code) {
		this.code = code;
	}

	public Violation getViolation() {
		return violation;
	}

	public void setViolation(Violation violation) {
		this.violation = violation;
	}

	public List<Picture> getPictures() {
		return pictures;
	}

	public void setPictures(List<Picture> pictures) {
		this.pictures = pictures;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public User getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(User modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp timestamp) {
		this.creationDate = timestamp;
	}

	public Timestamp getModifiedTimeStamp() {
		return modifiedTimeStamp;
	}

	public void setModifiedTimeStamp(Timestamp modifiedTimeStamp) {
		this.modifiedTimeStamp = modifiedTimeStamp;
	}
	
	
}
