package org.charleroi.provis.entity;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="code",schema="provis")
public class Code {
	@Id
	@Column(name = "code_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="code_num")
	private String num;
	
	@Column(name="code_desc")
	private String desc;
	
	@Column(name="code_status")
	private String status;
	
	@ManyToOne
	@JoinColumn(name = "code_creator_id")
	private User creator;
	
	@ManyToOne
	@JoinColumn(name = "code_modified_id")
	private User modifiedBy;
	
	@Column(name="code_creation_ts")
	private Timestamp creationDate;
	
	@Column(name="code_modified_ts")
	private Timestamp modifiedTimeStamp;

	public Code() {
		super();
	}

	public Code(Long id, String num, String desc, String status, User creator,
			User modifiedBy, Timestamp creationDate, Timestamp modifiedTimeStamp) {
		super();
		this.id = id;
		this.num = num;
		this.desc = desc;
		this.status = status;
		this.creator = creator;
		this.modifiedBy = modifiedBy;
		this.creationDate = creationDate;
		this.modifiedTimeStamp = modifiedTimeStamp;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public User getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(User modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp timestamp) {
		this.creationDate = timestamp;
	}

	public Timestamp getModifiedTimeStamp() {
		return modifiedTimeStamp;
	}

	public void setModifiedTimeStamp(Timestamp modifiedTimeStamp) {
		this.modifiedTimeStamp = modifiedTimeStamp;
	}

	@Override
	public String toString() {
		return "Code [id=" + id + ", num=" + num + ", desc=" + desc
				+ ", status=" + status + ", creator=" + creator
				+ ", modifiedBy=" + modifiedBy + ", creationDate="
				+ creationDate + ", modifiedTimeStamp=" + modifiedTimeStamp
				+ "]";
	}
	
}
