package org.charleroi.provis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="usertypes",schema="provis")
public class UserType {
	@Id
	@Column(name="userTypes_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="userTypes_key")
	private String key;
	
	@Column(name="userTypes_desc")
	private String description;
	
	@Column(name="userTypes_status")
	private String status;

	public UserType() {
		super();
	}

	public UserType(Long id, String key, String description, String status) {
		super();
		this.id = id;
		this.key = key;
		this.description = description;
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "UserType [id=" + id + ", key=" + key + ", description="
				+ description + ", status=" + status + "]";
	}
	
	
}
