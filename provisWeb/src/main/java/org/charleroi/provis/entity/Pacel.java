package org.charleroi.provis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="parcel",schema="provis")
public class Pacel {
	@Id
	@Column(name="PARCEL_ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="DISTRCIT")
	private String district;
	
	@Column(name="TAX_CODE_CURR")
	private String taxCode;
	
	@Column(name="NAME_1")
	private String lastName;
	
	@Column(name="NAME_2")
	private String firstName;
	
	@Column(name="ADDR_1")
	private String streetAddress;
	
	@Column(name="ADDR_2")
	private String cityCounty;
	
	@Column(name="ZIP_CODE")
	private String zipCode;
	
	@Column(name="SALE_DEED")
	private String saleDeed;
	
	@Column(name="DESC_1")
	private String desc1;
	
	@Column(name="DESC_2")
	private String desc2;
	
	@Column(name="DESC_3")
	private String desc3;
	
	@Column(name="AV_LAND_CURR")
	private String avLandCurr;
	
	@Column(name="AV_BLDG_CURR")
	private String avBldgCurr;

	public Pacel() {
		super();
	}

	public Pacel(Long id, String district, String taxCode,
			String lastName, String firstName, String streetAddress,
			String cityCounty, String zipCode, String saleDeed, String desc1,
			String desc2, String desc3, String avLandCurr, String avBldgCurr) {
		super();
		this.id = id;
		this.district = district;
		this.taxCode = taxCode;
		this.lastName = lastName;
		this.firstName = firstName;
		this.streetAddress = streetAddress;
		this.cityCounty = cityCounty;
		this.zipCode = zipCode;
		this.saleDeed = saleDeed;
		this.desc1 = desc1;
		this.desc2 = desc2;
		this.desc3 = desc3;
		this.avLandCurr = avLandCurr;
		this.avBldgCurr = avBldgCurr;
	}

	public Long getUser_id() {
		return id;
	}

	public void setUser_id(Long user_id) {
		this.id = user_id;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getCityCounty() {
		return cityCounty;
	}

	public void setCityCounty(String cityCounty) {
		this.cityCounty = cityCounty;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getSaleDeed() {
		return saleDeed;
	}

	public void setSaleDeed(String saleDeed) {
		this.saleDeed = saleDeed;
	}

	public String getDesc1() {
		return desc1;
	}

	public void setDesc1(String desc1) {
		this.desc1 = desc1;
	}

	public String getDesc2() {
		return desc2;
	}

	public void setDesc2(String desc2) {
		this.desc2 = desc2;
	}

	public String getDesc3() {
		return desc3;
	}

	public void setDesc3(String desc3) {
		this.desc3 = desc3;
	}

	public String getAvLandCurr() {
		return avLandCurr;
	}

	public void setAvLandCurr(String avLandCurr) {
		this.avLandCurr = avLandCurr;
	}

	public String getAvBldgCurr() {
		return avBldgCurr;
	}

	public void setAvBldgCurr(String avBldgCurr) {
		this.avBldgCurr = avBldgCurr;
	}

	@Override
	public String toString() {
		return "Pacel [id=" + id + ", district=" + district
				+ ", taxCode=" + taxCode + ", lastName=" + lastName
				+ ", firstName=" + firstName + ", streetAddress="
				+ streetAddress + ", cityCounty=" + cityCounty + ", zipCode="
				+ zipCode + ", saleDeed=" + saleDeed + ", desc1=" + desc1
				+ ", desc2=" + desc2 + ", desc3=" + desc3 + ", avLandCurr="
				+ avLandCurr + ", avBldgCurr=" + avBldgCurr + "]";
	}
	
}
