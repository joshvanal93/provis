package org.charleroi.provis.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ChainHandler {

	@Before("execution(* org.charleroi.provis.services..*(..))")
	public void logServiceBefore(JoinPoint joinPoint) {
		System.out.println("Entering Service :"  +joinPoint.getSignature().getDeclaringTypeName()+"."+ joinPoint.getSignature().getName());
	}
	
	@Before("execution(* org.charleroi.provis.dao..*(..))")
	public void logDAOBefore(JoinPoint joinPoint) {
		System.out.println("Entering DAO :" +joinPoint.getSignature().getDeclaringTypeName()+"."+ joinPoint.getSignature().getName());
	}
	
	@Before("execution(* org.charleroi.provis.factory..*(..))")
	public void logFactoryBefore(JoinPoint joinPoint) {
		System.out.println("Entering Factory :"  +joinPoint.getSignature().getDeclaringTypeName()+"."+ joinPoint.getSignature().getName());
	}
}
