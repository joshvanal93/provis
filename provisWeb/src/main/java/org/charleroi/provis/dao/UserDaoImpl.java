package org.charleroi.provis.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.charleroi.provis.entity.User;
import org.charleroi.provis.entity.UserType;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class UserDaoImpl {
	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<User> findAll() {
		return (List<User>) sessionFactory.getCurrentSession()
				.createCriteria(User.class).list();
	}

	public User findUserByLoginAndPassword(User userObj) {
		return (User) sessionFactory
				.getCurrentSession()
				.createCriteria(User.class)
				.add(Restrictions.eq("user_login", userObj.getUser_login()))
				.add(Restrictions.eq("user_password",
						userObj.getUser_password())).uniqueResult();
	}
	
	public User findUserByUserID(User userObj){
		return (User) sessionFactory
				.getCurrentSession()
				.createCriteria(User.class)
				.add(Restrictions.eq("user_id", userObj.getUser_id())).uniqueResult();
	}

	public User createUser(User userObj) {
		userObj.setUser_type((UserType)sessionFactory.getCurrentSession().createCriteria(UserType.class).add(Restrictions.eqOrIsNull("key", userObj.getUser_type().getKey())).uniqueResult());
		sessionFactory.getCurrentSession().saveOrUpdate(userObj);
		return userObj;
	}
	
	public boolean deleteUser(User userObj){
		sessionFactory.getCurrentSession().delete(userObj);
		return true;
	}
}