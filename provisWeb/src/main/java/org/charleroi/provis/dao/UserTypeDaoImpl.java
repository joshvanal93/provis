package org.charleroi.provis.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.charleroi.provis.entity.UserType;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class UserTypeDaoImpl {

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public List<UserType> findAll(){
		return sessionFactory.getCurrentSession().createCriteria(UserType.class).list();
	}
	
	public UserType insertOrUpdate(UserType userType){
		sessionFactory.getCurrentSession().saveOrUpdate(userType);
		System.out.println(sessionFactory.getCurrentSession().getTransaction().wasRolledBack());
		System.out.println(sessionFactory.getCurrentSession().getTransaction().wasCommitted());
		return userType;
	}
	
	public boolean deleteUserType(UserType userType){
		sessionFactory.getCurrentSession().delete(userType);
		return sessionFactory.getCurrentSession().getTransaction().wasCommitted();
	}
}
