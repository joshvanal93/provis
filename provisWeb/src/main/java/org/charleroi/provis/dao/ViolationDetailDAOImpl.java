package org.charleroi.provis.dao;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import javax.transaction.Transactional;

import org.charleroi.provis.entity.User;
import org.charleroi.provis.entity.ViolationDetail;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class ViolationDetailDAOImpl {

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	UserDaoImpl userDao;

	@SuppressWarnings("unchecked")
	public List<ViolationDetail> findAll() {
		return sessionFactory.getCurrentSession()
				.createCriteria(ViolationDetail.class).list();
	}

	public ViolationDetail findById(Long id) {
		return (ViolationDetail) sessionFactory.getCurrentSession()
				.createCriteria(ViolationDetail.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}
	
	public ViolationDetail insertOrUpdate(ViolationDetail detail, User user){
		if(detail.getId() == null){
			detail.setCreationDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			detail.setCreator(user);
		}
		detail.setModifiedTimeStamp(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		detail.setModifiedBy(user);
		sessionFactory.getCurrentSession().saveOrUpdate(detail);
		
		return detail;
	}
}
