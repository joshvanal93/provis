package org.charleroi.provis.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.charleroi.provis.entity.Pacel;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class PacelDaoImpl {
	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<Pacel> findAll() {
		return (List<Pacel>) sessionFactory.getCurrentSession()
				.createCriteria(Pacel.class).list();
	}

	public Pacel findById(Long id) {
		return (Pacel) sessionFactory.getCurrentSession()
				.createCriteria(Pacel.class).add(Restrictions.eq("id", id))
				.uniqueResult();
	}

	public Pacel insertOrUpdate(Pacel pacel) {
		sessionFactory.getCurrentSession().saveOrUpdate(pacel);
		return pacel;
	}

	public boolean deletePacel(Pacel pacel) {
		sessionFactory.getCurrentSession().delete(pacel);
		return true;
	}

}
