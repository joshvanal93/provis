package org.charleroi.provis.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.charleroi.provis.entity.DBConfig;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class DBConfigDAO {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<DBConfig> findAll() {
		return sessionFactory.getCurrentSession()
				.createCriteria(DBConfig.class).list();
	}

	@SuppressWarnings("unchecked")
	public List<DBConfig> findAllActive() {
		return (List<DBConfig>) sessionFactory.getCurrentSession()
				.createCriteria(DBConfig.class)
				.add(Restrictions.eq("status", 'A')).list();
	}

	public DBConfig findByKey(String key) {
		return (DBConfig) sessionFactory.getCurrentSession()
				.createCriteria(DBConfig.class)
				.add(Restrictions.eq("key", key)).uniqueResult();
	}

	public DBConfig insertOrUpdate(DBConfig obj) {
		if(obj.getId() == null){
			obj.setStatus("A");
		}
		sessionFactory.getCurrentSession().saveOrUpdate(obj);
		return obj;
	}
	
	public boolean remove(DBConfig obj){
		sessionFactory.getCurrentSession().delete(obj);
		return true;
	}
}
