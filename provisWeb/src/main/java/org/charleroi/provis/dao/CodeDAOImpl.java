package org.charleroi.provis.dao;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import javax.transaction.Transactional;

import org.charleroi.provis.entity.Code;
import org.charleroi.provis.entity.User;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class CodeDAOImpl {
	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	UserDaoImpl userDao;
	
	@SuppressWarnings("unchecked")
	public List<Code> findAll() {
		return sessionFactory.getCurrentSession().createCriteria(Code.class)
				.list();
	}

	@SuppressWarnings("unchecked")
	public List<Code> findAllActive() {
		return sessionFactory.getCurrentSession().createCriteria(Code.class)
				.add(Restrictions.eq("status", "A")).list();
	}

	public Code findById(Long id) {
		return (Code) sessionFactory.getCurrentSession()
				.createCriteria(Code.class).add(Restrictions.eq("id", id))
				.uniqueResult();
	}

	public Code findByNum(String num) {
		return (Code) sessionFactory.getCurrentSession()
				.createCriteria(Code.class).add(Restrictions.eq("num", num))
				.uniqueResult();
	}

	public Code findByIdAndActive(Long id) {
		return (Code) sessionFactory.getCurrentSession()
				.createCriteria(Code.class).add(Restrictions.eq("id", id))
				.add(Restrictions.eq("status", "A")).uniqueResult();
	}

	public Code findByNumAndActive(String num) {
		return (Code) sessionFactory.getCurrentSession()
				.createCriteria(Code.class).add(Restrictions.eq("num", num))
				.add(Restrictions.eq("status", "A")).uniqueResult();
	}
	
	public Code insertOrUpdate(Code code,User user){
		if(code.getId() == null){
			code.setCreator(user);
			code.setCreationDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			if(code.getStatus() == null){
				code.setStatus("A");
			}
		}
		code.setModifiedBy(user);
		code.setModifiedTimeStamp(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		
		sessionFactory.getCurrentSession().saveOrUpdate(code);
		
		return code;
	}
	
	public boolean deleteCode(Code code){
		sessionFactory.getCurrentSession().delete(code);
		return true;
	}
}
