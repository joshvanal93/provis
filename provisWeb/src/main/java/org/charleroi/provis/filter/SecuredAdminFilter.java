package org.charleroi.provis.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.charleroi.provis.dao.UserDaoImpl;
import org.charleroi.provis.dao.UserTypeDaoImpl;
import org.charleroi.provis.entity.User;
import org.charleroi.provis.providers.ApplicationContextProvider;

public class SecuredAdminFilter extends AbstractFilter implements Filter {
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		User userObj = (User) request.getAttribute("userObj");
		UserDaoImpl userDao = ApplicationContextProvider.getApplicationContext().getBean("userDaoImpl",UserDaoImpl.class);
		if(userObj != null){
			if(userDao.findUserByUserID(userObj).getUser_type().getKey().equals("A")){
				chain.doFilter(request, response);
			}else{
				unauthorized((HttpServletResponse) response);
			}
		}else{
			unauthorized((HttpServletResponse) response);
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
	

}
