package org.charleroi.provis.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.charleroi.provis.entity.User;
import org.charleroi.provis.factory.UserFactory;
import org.springframework.stereotype.Component;

@Component
public class SecuredFilter extends AbstractFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		try{
			HttpServletRequest req = (HttpServletRequest) request;
			Cookie[] userCookies = req.getCookies();
			User userToken = UserFactory.create(userCookies);
			if(userToken != null){
				request.setAttribute("userObj", userToken);
				chain.doFilter(request, response);
			}else{
				unauthorized((HttpServletResponse)response);
			}
		}catch(Exception e){
			unauthorized((HttpServletResponse) response);
		}
	}

	@Override
	public void destroy() {}

}
