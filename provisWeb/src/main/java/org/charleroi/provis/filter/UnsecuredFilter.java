package org.charleroi.provis.filter;

import java.io.IOException;
import java.util.StringTokenizer;

import org.charleroi.provis.entity.User;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

@Component
public class UnsecuredFilter extends AbstractFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp;
		if(req.getPathInfo().compareTo("/unsecured/login") == 0){
			String authHeader = req.getHeader("Authorization");
			if(authHeader != null){
				StringTokenizer tokens = new StringTokenizer(authHeader);
				if(tokens.countTokens() == 2){
					tokens.nextToken();
					String base64 = tokens.nextToken();
					byte[] user_pass = DatatypeConverter.parseBase64Binary(base64);
					StringTokenizer userTokens = new StringTokenizer(new String(user_pass), ":");
					User userobj = new User();
					userobj.setUser_login(userTokens.nextToken());
					userobj.setUser_password(userTokens.nextToken());
					request.setAttribute("userObj", userobj);
					resp = (HttpServletResponse) response;
					chain.doFilter(request,response);
			
				}else{
					unauthorized((HttpServletResponse) response);
				}	
			}else{
				unauthorized((HttpServletResponse) response);
			}
		}else{
			unauthorized((HttpServletResponse) response);
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
}
