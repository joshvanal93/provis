package org.charleroi.provis.filter;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

public abstract class AbstractFilter {
	protected void unauthorized(HttpServletResponse resp)throws IOException, ServletException{
		resp.sendError(403,"Unauthorized");
	}
}
