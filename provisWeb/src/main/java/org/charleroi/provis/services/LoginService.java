package org.charleroi.provis.services;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.charleroi.provis.dao.UserDaoImpl;
import org.charleroi.provis.entity.User;
import org.charleroi.provis.factory.UserFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="unsecured/login")
public class LoginService {

	@Autowired
	UserDaoImpl userDao;
	
	@RequestMapping(method=RequestMethod.GET)
	public void login(HttpServletRequest request,HttpServletResponse response) throws NoSuchAlgorithmException, IOException, LoginException{
		User userObj = (User) request.getAttribute("userObj");
		userObj.setUser_password(UserFactory.EncryptPassword(userObj.getUser_password()));
		userObj = userDao.findUserByLoginAndPassword(userObj);
		if(userObj != null){
			response.addCookie(UserFactory.createCookie(userObj, request));
		}else{
			throw new LoginException("invalid information");
		}
	}

}
