package org.charleroi.provis.services;

import java.util.List;

import org.charleroi.provis.dao.UserDaoImpl;
import org.charleroi.provis.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="secured/user")
public class SecuredUserService {
	@Autowired
	UserDaoImpl userDao;
	
	@RequestMapping(method=RequestMethod.GET,produces="application/json")
	public List<User> getUserPermissions(){
		
	}
}
