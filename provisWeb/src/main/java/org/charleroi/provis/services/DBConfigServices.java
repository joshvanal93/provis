package org.charleroi.provis.services;

import java.util.List;

import org.charleroi.provis.dao.DBConfigDAO;
import org.charleroi.provis.entity.DBConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="secured/admin/dbConfig")
public class DBConfigServices {

	@Autowired
	DBConfigDAO dbConfigDAO;
	
	@RequestMapping(method=RequestMethod.GET,produces="application/json")
	public List<DBConfig> getAllDBConfig(){
		return dbConfigDAO.findAll();
	}
	@RequestMapping(value="/{key}",method=RequestMethod.GET,produces="application/json")
	public DBConfig getAllDBConfig(@PathVariable(value="key")String key){
		return dbConfigDAO.findByKey(key);
	}
	
	@RequestMapping(value="/active", method=RequestMethod.GET,produces="application/json")
	public List<DBConfig> getAllActiveDBConfig(){
		return dbConfigDAO.findAllActive();
	}
	
	@RequestMapping(method={RequestMethod.POST,RequestMethod.PUT}, consumes="application/json",produces="application/json")
	public DBConfig insertDBConfig(@RequestBody DBConfig dbConfig){
		dbConfig = dbConfigDAO.insertOrUpdate(dbConfig);
		return dbConfig;
	}
	
	@RequestMapping(method = RequestMethod.DELETE, consumes="application/json",produces="application/json")
	public boolean deleteDBConfig(@RequestBody DBConfig dbConfig){
		return dbConfigDAO.remove(dbConfig);
	}
}
