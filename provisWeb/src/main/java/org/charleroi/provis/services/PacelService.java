package org.charleroi.provis.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.charleroi.provis.dao.PacelDaoImpl;
import org.charleroi.provis.entity.Code;
import org.charleroi.provis.entity.Pacel;
import org.charleroi.provis.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "secured/pacel")
public class PacelService {

	@Autowired
	PacelDaoImpl pacelDao;
	
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public List<Pacel> findAll() {
		return pacelDao.findAll();
	}

	@RequestMapping(method = { RequestMethod.POST, RequestMethod.PUT }, produces = "application/json", consumes = "application/json")
	public Pacel insertOrUpdateCode(@RequestBody Pacel pacel,
			HttpServletRequest request) {
		pacel = pacelDao.insertOrUpdate(pacel);
		return pacel;
	}

	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json", consumes = "application/json")
	public boolean deleteCode(@RequestBody Pacel pacel) {
		return pacelDao.deletePacel(pacel);
	}

}
