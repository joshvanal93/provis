package org.charleroi.provis.services;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.charleroi.provis.dao.UserDaoImpl;
import org.charleroi.provis.entity.User;
import org.charleroi.provis.factory.UserFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="secured/admin/user")
public class UserServices {

	@Autowired
	UserDaoImpl userDao;
	
	@RequestMapping(method=RequestMethod.GET,produces="application/json")
	public List<User> getAllUsers(){
		return userDao.findAll();
	}
	
	@RequestMapping(method={RequestMethod.POST,RequestMethod.PUT},produces="application/json", consumes="application/json")
	public User createUser(@RequestBody User userObj) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		userObj.setUser_password(UserFactory.EncryptPassword(userObj.getUser_password()));
		userObj = userDao.createUser(userObj);
		return userObj;
	}
}
