package org.charleroi.provis.services;

import java.util.List;

import org.charleroi.provis.dao.UserTypeDaoImpl;
import org.charleroi.provis.entity.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="secured/admin/userTypes")
public class UserTypeServices {
	
	@Autowired
	private UserTypeDaoImpl userTypeDao;
	
	@RequestMapping(method=RequestMethod.GET, produces="application/json")
	public List<UserType> findAll(){
		return userTypeDao.findAll();
	}
	
	@RequestMapping(method={RequestMethod.POST,RequestMethod.PUT},produces="application/json",consumes="application/json")
	public UserType insertOrUpdate(@RequestBody UserType userType){
		if(userType.getStatus() == null){
			userType.setStatus("A");
		}
		
		return userTypeDao.insertOrUpdate(userType);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, produces="application/json",consumes="application/json")
	public boolean deleteUserType(@RequestBody UserType userType){
		return userTypeDao.deleteUserType(userType);
	}
}
