package org.charleroi.provis.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.charleroi.provis.dao.CodeDAOImpl;
import org.charleroi.provis.dao.UserDaoImpl;
import org.charleroi.provis.entity.Code;
import org.charleroi.provis.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "secured/code")
public class CodeServices {
	@Autowired
	private UserDaoImpl userDao;

	@Autowired
	private CodeDAOImpl codeDao;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public List<Code> findAll() {
		return codeDao.findAll();
	}

	@RequestMapping(method = { RequestMethod.POST, RequestMethod.PUT }, produces = "application/json", consumes = "application/json")
	public Code insertOrUpdateCode(@RequestBody Code code,
			HttpServletRequest request) {
		User userObj = (User) request.getAttribute("userObj");
		userObj = userDao.findUserByUserID(userObj);
		if (code.getId() != null) {
			Code oldCode = codeDao.findById(code.getId());
			code.setCreationDate(oldCode.getCreationDate());
			code.setCreator(oldCode.getCreator());
		}
		code = codeDao.insertOrUpdate(code, userObj);
		return code;
	}

	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json", consumes = "application/json")
	public boolean deleteCode(@RequestBody Code code) {
		return codeDao.deleteCode(code);
	}

}