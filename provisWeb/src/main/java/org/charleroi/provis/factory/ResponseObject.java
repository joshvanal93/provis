package org.charleroi.provis.factory;

public class ResponseObject {
	private String responseTest;

	public String getResponseTest() {
		return responseTest;
	}

	public void setResponseTest(String responseTest) {
		this.responseTest = responseTest;
	}

}
