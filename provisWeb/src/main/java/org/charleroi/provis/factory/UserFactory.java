package org.charleroi.provis.factory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.charleroi.provis.entity.User;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class UserFactory {
	private static String userCookieKey = "provisToken";

	public static User create(Cookie[] userCookies) throws JsonParseException,
			JsonMappingException, IOException {
		// TODO Auto-generated method stub
		Cookie userToken = null;
		User userObj = null;
		for (int i = 0; i < userCookies.length && (userToken == null); i++) {
			if (userCookies[i].getName().equals(userCookieKey)) {
				userToken = userCookies[i];
			}
		}

		if (userToken != null) {
			ObjectMapper mapper = new ObjectMapper();
			
			userObj = mapper.readValue(new Base64().decode(URLDecoder.decode(userToken.getValue(), "UTF-8")),
					User.class);
		}
		return userObj;
	}

	public static String EncryptPassword(String password)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		StringBuilder sb = new StringBuilder();
		if (password != null) {
			digest.update(password.getBytes("UTF-8"));
			byte[] bytes = digest.digest();
			for (int j = 0; j < bytes.length; j++) {
				sb.append(Integer.toString((bytes[j] & 0xff) + 0x100, 16)
						.substring(1));
			}
		}

		return sb.toString();
	}

	public static Cookie createCookie(User userObj, HttpServletRequest request)
			throws JsonProcessingException, UnsupportedEncodingException {
		ObjectMapper mapper = new ObjectMapper();
		Cookie userToken = new Cookie(userCookieKey, URLEncoder.encode(new String(new Base64().encode(mapper
				.writeValueAsBytes(userObj))),"UTF-8"));
		userToken.setDomain(request.getServerName());
		userToken.setMaxAge(60 * 60 * 24);
		userToken.setPath("/");
		return userToken;
	}

}
