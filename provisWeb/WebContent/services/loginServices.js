module.exports = function(app){
  app.service('LoginService',["$http",function($http){
    this.getLoginCooke = function(userInfo){
       $http.defaults.headers.common['Authorization'] = 'Basic ' + window.btoa(userInfo.userName+":"+userInfo.password);
      return $http({
        method:"GET",
        url:"/services/unsecured/login"
      })
    }
  }]);
}
